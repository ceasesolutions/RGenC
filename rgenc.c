//RGenC is a configurable random number generator created by the Cease Solutions Project in 46 MLOC.
//RGenC is licensed under the GNU AFFERO GENERAL PUBLIC LICENSE.
//
//Contacts for any questions:
//kotagi@tutanota.com
//https://discord.gg/rHJJbAuRZD
//
//TODO:
//Add more comments to help normal people understand whats going on
//Make more efficient
//DO NOT ADD ERROR HANDLING. Any retard who can't give reasonable parameters deserves what comes to them
//
//Compile: gcc -o rgenc rgenc.c
//
//USAGE:
//command is rgenc. It accepts flags -l, -h, and -c
//-l is the lowest number it may generate
//-h is the highest number it may generate
//-c is the amount of numbers to generate
//Example: rgenc -l 40 -h 80 -c 6
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>

int main(int argc, char *argv[]) {
    int32_t lower = 1;
    int32_t higher = 100;
    int32_t count = 1;

    for (int i = 1; i < argc - 1; i += 2) {
        if (argv[i][0] == '-' && argv[i][1] == 'l') {
            lower = strtol(argv[i + 1], NULL, 10);
        } else if (argv[i][0] == '-' && argv[i][1] == 'h') {
            higher = strtol(argv[i + 1], NULL, 10);
        } else if (argv[i][0] == '-' && argv[i][1] == 'c') {
            count = strtol(argv[i + 1], NULL, 10);
        }
    }

    srand(time(NULL));
    uint32_t mt[624];
    uint32_t index = 0;
    mt[0] = time(NULL);

    for (int i = 1; i < 624; i++) {
        mt[i] = (1812433253UL * (mt[i - 1] ^ (mt[i - 1] >> 30)) + i);
    }

    for (int i = 0; i < count; i++) {
        if (index == 0) {
            for (int j = 0; j < 624; j++) {
                uint32_t y = (mt[j] & 0x80000000UL) + (mt[(j + 1) % 624] & 0x7fffffffUL);
                mt[j] = mt[(j + 397) % 624] ^ (y >> 1);

                if (y % 2 != 0) {
                    mt[j] ^= 0x9908b0dfUL;
                }
            }
        }

        uint32_t random = mt[index];
        random ^= (random >> 11);
        random ^= (random << 7) & 0x9d2c5680UL;
        random ^= (random << 15) & 0xefc60000UL;
        random ^= (random >> 18);
        index = (index + 1) % 624;
        int32_t scaled = (int32_t)((double)random / UINT32_MAX * (higher - lower + 1) + lower);
        printf("%d\n", scaled);
    }

    return 0;
}
